@extends('layouts.app')

@section('content')
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset('/custom/fonts/material-icon/css/material-design-iconic-font.min.css')}}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('/custom/css/style.css')}}">
</head>
<body>

    <div class="main">

        <!-- Sing in  Form -->
        <section class="sign-in">
            <div class="container">
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src="/custom/images/signin-image.jpg" alt="sing up image"></figure>
                        <!-- <a href="/new-register" class="signup-image-link">Create an account</a> -->
                    </div>

                    <div class="signin-form">

                        <h2 class="form-title">Hi</h2> 
						
						<h3 class="form-title"><?php echo strtoupper(Auth::user()->name)?> </h3>
						<h3 class="form-title">Your are Logged in</h3> 
                        <div class="form-group form-button">
						
                            
                            </div>
								<button  class="form-submit"
                                    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                </button> 
						
                    </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </section>

		</div>
		
		
		

    <!-- JS -->
    
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

@endsection
